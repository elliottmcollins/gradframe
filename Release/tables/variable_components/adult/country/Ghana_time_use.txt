		(1)	(2)	(3)	(4)	(5)	(6)	(7)	(8)	(9)	(10)
VARIABLES	LABELS	time_work_end	time_agri_end	time_animals_end	time_business_end	time_paidlabor_end	time_work_fup	time_agri_fup	time_animals_fup	time_business_fup	time_paidlabor_fup
											
treatment	Treatment Status: ITT	5.28	0.82	1.99***	1.75	-0.24	10.0	10.9	0.97	2.65	-3.73
		(11.2)	(9.40)	(0.63)	(8.00)	(1.89)	(11.7)	(10.8)	(0.77)	(6.18)	(2.77)
Constant	Constant	157***	108***	-1.00	41.8**	11.1	242***	176***	2.60**	48.2***	20.4***
		(32.6)	(28.8)	(0.79)	(18.4)	(7.95)	(38.8)	(37.0)	(1.31)	(12.9)	(6.57)
											
Observations		2,377	2,377	2,377	2,377	2,376	2,279	2,279	2,279	2,279	2,279
R-squared		0.174	0.141	0.085	0.096	0.097	0.188	0.195	0.061	0.082	0.054
Control mean		147	102	0.36	38.5	6.69	171	136	0.86	27.2	6.91
Baseline mean		0	0	0	0	0	0	0	0	0	0
Robust standard errors in parentheses											
*** p<0.01, ** p<0.05, * p<0.1											
