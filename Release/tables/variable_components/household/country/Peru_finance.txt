	(1)	(2)	(3)	(4)	(5)	(6)	(7)	(8)	(9)	(10)
VARIABLES	loan_totalamt_end	loan_informalamt_end	loan_formalamt_end	sav_totalamt_end	sav_depositamt_end	loan_totalamt_fup	loan_informalamt_fup	loan_formalamt_fup	sav_totalamt_fup	sav_depositamt_fup
										
treatment	28.1	25.5	2.64	1.97	-0.56	-52.4	-60.1	11.7	45.1*	2.56
	(44.2)	(42.6)	(14.2)	(34.1)	(4.02)	(57.8)	(56.9)	(11.9)	(23.8)	(4.30)
Constant	-107	-112	-0.19	-109	3.98	-129	-127	12.2	60.6	35.2***
	(157)	(152)	(28.6)	(89.7)	(7.81)	(115)	(110)	(39.5)	(65.9)	(11.0)
										
Observations	2,101	2,101	2,101	2,101	2,056	2,016	2,016	2,016	2,017	2,017
R-squared	0.068	0.056	0.027	0.050	0.047	0.071	0.069	0.025	0.053	0.064
Control mean	227	194	30.5	266	27.7	291	271	14.8	175	42.9
Baseline mean	126	112	12.5	42.4	14.2	126	112	12.5	42.4	14.2
Robust standard errors in parentheses										
*** p<0.01, ** p<0.05, * p<0.1										
