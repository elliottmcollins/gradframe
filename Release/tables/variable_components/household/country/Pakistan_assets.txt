	(1)	(2)	(3)	(4)	(5)	(6)	(7)	(8)	(9)
VARIABLES	asset_index_end	asset_prod_index_end	asset_hh_index_end	asset_index_fup	asset_tot_value_fup	asset_prod_index_fup	asset_prod_value_fup	asset_hh_index_fup	asset_hh_value_fup
									
treatment	0.33***	0.35***	0.072	0.17**	270***	0.18**	163***	0.039	6.55
	(0.063)	(0.066)	(0.052)	(0.067)	(103)	(0.068)	(62.5)	(0.051)	(7.61)
Constant	-0.0046	-0.0071	0.025	-0.0011	925***	0.0030	443***	0.00056	149***
	(0.042)	(0.043)	(0.041)	(0.042)	(65.3)	(0.043)	(39.9)	(0.037)	(5.31)
									
Observations	1,083	1,083	1,054	1,027	1,017	1,027	1,027	1,017	1,017
R-squared	0.184	0.164	0.360	0.149	0.118	0.140	0.121	0.411	0.323
Control mean	-8.1e-11	-5.8e-09	-6.2e-09	-1.9e-09	918	-4.5e-09	436	-5.3e-09	150
Baseline mean	0.033	0.036	0.0070	0.033	0	0.036	0	0.0070	0
Robust standard errors in parentheses									
*** p<0.01, ** p<0.05, * p<0.1									
