Analysis of the Graduation Framework
====================================

Date: 2015-12-08

This repository tracks an attempt at a new analysis of the data from Banerjee et
al.'s 2015 paper analyzing the results of six different randomized evaluations
of the so-called "Graduation Framework" for alleviating poverty and increasing
productivity among particularly poor households. This project is open-ended, and
has multiple related goals. A partial list of objectives includes:

1. Produce a high-quality replication of the original results in Banerjee et al. (2015)
2. Evaluate the welfare impacts of the graduation framework using the more
   structural methods described in Ligon (2015) *(not yet included here)* and
   Collins & Ligon (2015), especially the so-called "neediness" index.
3. Use the relatively relaxed data requirements of the neediness index to
   integrate other datasets into the analysis that would otherwise be difficult
   to compare.
4. Examine heterogeneity of treatment effects, especially w.r.t. the relative
   impacts on consumption and investment.

An important step will be to get more disaggregated versions of certain modules,
especially consumption and asset ownership.

NOTES:
------

**Release** contains the publicly available data and documents made available
along with the paper published in Science. Data and code used for new analysis gets
copied over to the **data** directory, so new and original work is easily separated.
